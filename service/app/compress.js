/* 
使用工具：https://tinypng.com
功能：将png文件进行压缩
使用方法：
    如下示例
*/

const config = require("../../config/config");
const ImageHandler = require('../lib/image');

try {
    var handler = new ImageHandler(
        config.COMPRESS_CONF.source,
        config.COMPRESS_CONF.dist
    );
    handler.setLoggerStatus(true);
    handler.convertImages();
} catch(e) {
    console.log(e);
}