/*
    webp图片生成
    
    运行：npm install && npm start
    程序依赖谷歌官方webp转换工具cwebp
    mac下安装 brew install webp
    windows下可以去google官方下载
    安装完成后运行cwebp -h 如果显示了使用帮助则表示安装成功
 */
const config = require("../../config/config");
const Watcher = require("../lib/watcher");

const fs   = require('fs');
const webp   = require('webp-converter');
const PATH = require('path');

const constSourceFiles = /^\.webp|\.jpg|\.jpeg|\.png/;     // 非webp的图片
const ignoreFiles = /(^\..+)|(.+[\/\\]\..+)|(.+?\.webp$)/; // 忽略文件.开头和.webp结尾的

const webpConf  = config.WEBP_CONVERT_CONF;
const sourceDir = webpConf.source;
const distDir   = webpConf.dist;
const quality   = webpConf.quality;

var getWebpImgName = (path) => {
    return `${path}.webp`.split(PATH.sep).join('/').replace(sourceDir, distDir);
}

var deleteWebpImg = (path) => {
    let distPath = getWebpImgName(path);
    if (!fs.existsSync(distPath)) {
        return;
    }
    fs.unlink(distPath, (err) => {
        let textStatus = err ? "失败" : "成功";
        console.log(`删除图片 ${distPath} ${textStatus}`);
    });
}

const watcher = new Watcher(sourceDir, ignoreFiles);
watcher.watch((path) => {
    let distPath = getWebpImgName(path);
    if (!constSourceFiles.test(path)) {
        return;
    }
    webp.cwebp(path, distPath, `-q ${quality}`, (status) => {
        let textStatus = status.indexOf(100) !== false ? '成功' : '失败';
        console.log(`转换图片 ${distPath} ${textStatus}`);
    })
}, (path) => {
    deleteWebpImg(path);
});