const fs = require('fs');
const image = require('imageinfo');
const sysPath = require('path');
const tinify = require('tinify');
const config = require("../../config/config");
tinify.key = config.API_KEY;

class ImageHandler {
    constructor(sourcePath, distPath, filelist = [], isImage = true) {
        this.sourcePath = sourcePath;
        this.loggerStatus = false;
        var state = fs.statSync(distPath);
        if (!state.isDirectory()) {
            throw Error('invalid distPath, distPath should be a directory!');
        }
        this.distPath = distPath;
        this.filelist = filelist;
        this.isImage  = isImage;
    }
    mkdirsSync(dirname) {
        if (fs.existsSync(dirname)) {
            return true;
        } else {
            if (this.mkdirsSync(sysPath.dirname(dirname))) {
                fs.mkdirSync(dirname);
                return true;
            }
        }
    }
    setLoggerStatus(loggerStatus) {
        this.loggerStatus = loggerStatus;
    }
    resetFilelist() {
        this.filelist = [];
    }
    setGetFileType(isImage) {
        this.isImageType = isImage;
    }
    isFileImageType(path) {
        let ms = image(fs.readFileSync(path));
        return ms.mimeType;
    }
    readFileList(path) {
        var files = fs.readdirSync(path);
        files.forEach((filename, index) => {
            var fullpath = path + sysPath.sep + filename;
            var state = fs.statSync(fullpath);
            if (state.isDirectory()) {
                this.readFileList(fullpath + sysPath.sep);
            } else {
                let isFileAddedToList = true;
                if (this.isImage) {
                    isFileAddedToList = this.isFileImageType(fullpath);
                }
                if (isFileAddedToList) {
                    this.filelist.push({
                        filename: filename,
                        path: fullpath,
                        dist: fullpath.replace(this.sourcePath, this.distPath)
                    });
                }
            }
        });
    }
    getFileList() {
        this.readFileList(this.sourcePath);
        return this.filelist;
    }
    convertImage(frompath, topath) {
        if (!fs.existsSync(topath)) {
            var source = tinify.fromFile(frompath);
            this.mkdirsSync(sysPath.dirname(topath));
            source.toFile(topath);
            this.logger([
                `source: ${frompath}`,
                `dist: ${topath}`
            ].join("\t"));
        }
    }
    convertImages() {
        this.getFileList();
        if (this.filelist.length) {
            let length = this.filelist.length;
            this.logger(`find ${length} images`);
            this.filelist.forEach((img, index) => {
                this.logger([
                    `convert:`,
                    `${index + 1}/${length}`
                ].join("\t"));
                this.convertImage(img.path, img.dist);
            });
        }
    }
    logger(msg) {
        if (this.loggerStatus) {
            console.log(msg);
        }
    }
}

module.exports = ImageHandler;