/* 
监控文件变化

*/
const chokidar = require('chokidar');

class Watcher {    
    constructor (watchDir, ignoreFileRule) {
        this.watcher = chokidar.watch(watchDir, {
            ignored: path => {
                return ignoreFileRule.test(path);
            },
            persistent: true // 保持监听状态
        });
        this.log = console.log.bind(console);
    }

    watch(callbackAdd, callbackDel) {
        // 监听增加，修改，删除文件的事件
        this.watcher.on('all', (event, path) => {
            switch (event) {
                case 'add':
                case 'change':
                    callbackAdd(path);
                    break;
                case 'unlink':
                    callbackDel(path);
                    break;
                default:
                    break;
            }
        });
    }
}

module.exports = Watcher;