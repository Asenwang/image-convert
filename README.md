## 工具目录

```
|--config				配置文件目录
|--images				图片转换目录
	|--compress			文件压缩目录
	|--webp				webp文件转换目录
|--service				服务目录
	|--lib				lib库目录
	|--app				应用目录
    |--compress.js		压缩文件
		|--webp.js		webp文件转换
```

## 工具功能

```
1. 基于https://tinypng.com/进行图片压缩

图片压缩工具免费版 500张/月, 所以如果用完了就在 config.js 中换用新的key

2. 基于google提供的webp图片进行图片转换
下载地址https://developers.google.com/speed/webp/download
```



## 如何使用

```
下载工具安装依赖
git clone git@gitlab.com:Asenwang/image-convert.git && cd imageConvert && npm install

图片压缩
  1. 在https://tinypng.com/developers 注册获取API_KEY,并在config文件中配置
  2. npm run compress

图片转webp
  1. 下载安装webp转换工具
     下载地址https://developers.google.com/speed/webp/download
  2. npm run 2webp
```

